using Services.Claim.Models;

namespace Services.Claim;

public interface IClaimService
{
	Task<IEnumerable<InsuranceClaim>> GetClaimsAsync();
	Task<InsuranceClaim?> GetClaimAsync(string id);
	Task AddItemAsync(InsuranceClaim item);
	Task DeleteItemAsync(string id);
}