
using Services.Claim.Models;

namespace Services.Claim;

public class ClaimService : IClaimService
{
	private readonly IRepository<InsuranceClaim> _repository;
	public ClaimService(IRepository<InsuranceClaim> repository)
	{
		_repository = repository;
	}

	public async Task<IEnumerable<InsuranceClaim>> GetClaimsAsync()
	{
		return await _repository.GetAllAsync();
	}

	public async Task<InsuranceClaim?> GetClaimAsync(string id)
	{
		return await _repository.GetByIdAsync(id);
	}

	public Task AddItemAsync(InsuranceClaim item)
	{
		return _repository.AddItemAsync(item);
	}

	public Task DeleteItemAsync(string id)
	{
		return _repository.DeleteItemAsync(id);
	}
}