using Services.Audit.Models;

namespace Services.Interfaces;

public interface IAuditRepository
{
	Task AddAsync(AuditBase audit);
}