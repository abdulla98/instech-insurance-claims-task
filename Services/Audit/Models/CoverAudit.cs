﻿namespace Services.Audit.Models
{
    public class CoverAudit: AuditBase
    {
        public string? CoverId { get; set; }
    }
}
