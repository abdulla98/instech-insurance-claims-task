﻿namespace Services.Audit.Models
{
    public class ClaimAudit: AuditBase
    {
        public string? ClaimId { get; set; }
    }
}
