using Services.Interfaces;
using Services.Audit.Models;

namespace Infrastructure
{
    public class FileAuditRepository : IAuditRepository
    {
        private readonly string _filePath = "Logs/AuditLogFile.txt"; 

        public async Task AddAsync(AuditBase audit)
        {
            var auditData = $"{DateTime.UtcNow}: {audit}";
            await File.AppendAllTextAsync(_filePath, auditData + Environment.NewLine);
        }
    }
}
