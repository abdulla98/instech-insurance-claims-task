using Services.Cover.Models;

namespace Services.Cover;

public class CoverService: ICoverService
{
	private readonly IRepository<PolicyCover> _repository;

	public CoverService(IRepository<PolicyCover> repository)
	{
		_repository = repository;
	}
	
	public decimal ComputePremium(DateOnly startDate, DateOnly endDate, CoverType coverType)
	{
		return new PolicyCover(startDate, endDate, coverType).Premium;
	}

	public async Task<IEnumerable<PolicyCover>> GetAllCoversAsync()
	{ 
		return  await _repository.GetAllAsync();
	}

	public async Task<PolicyCover?> GetCoverByIdAsync(string id)
	{
		return await _repository.GetByIdAsync(id);
	}

	public async Task CreateCoverAsync(PolicyCover cover)
	{
		await _repository.AddItemAsync(cover);
	}

	public async Task DeleteCoverAsync(string id)
	{
		await _repository.DeleteItemAsync(id);
	}
}