namespace Services.Cover;

public class TankerStrategy : PremiumCalculator
{
	public const decimal TankerMultiplier = 1.5M; 
	public TankerStrategy(decimal basePremium) : base(basePremium) { }
	
	protected override decimal CoverTypeMultiplier { get; } = TankerMultiplier;
	protected override decimal EarlyPeriodDiscountMultiplier { get; } = 0.02M;
	protected override decimal ExtendedPeriodDiscountMultiplier { get; } = 0.03M;
}