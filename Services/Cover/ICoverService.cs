using Services.Cover.Models;

namespace Services.Cover;

public interface ICoverService
{
	decimal ComputePremium(DateOnly startDate, DateOnly endDate, CoverType coverType);
	Task<IEnumerable<PolicyCover>> GetAllCoversAsync();
	Task<PolicyCover?> GetCoverByIdAsync(string id);

	Task CreateCoverAsync(PolicyCover cover);
	Task DeleteCoverAsync(string id);
}