namespace Services.Cover;

public class PassengerShipStrategy: PremiumCalculator
{
	public const decimal PassengerShipMultiplier = 1.2M;
	public PassengerShipStrategy(decimal basePremium) : base(basePremium) { }
	protected override decimal CoverTypeMultiplier { get; } = PassengerShipMultiplier;
	protected override decimal EarlyPeriodDiscountMultiplier { get; } = 0.02M;
	protected override decimal ExtendedPeriodDiscountMultiplier { get; } = 0.03M;
}