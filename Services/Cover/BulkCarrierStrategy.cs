namespace Services.Cover;

public class BulkCarrierStrategy: PremiumCalculator
{
	public BulkCarrierStrategy(decimal basePremium) : base(basePremium) { }
	protected override decimal CoverTypeMultiplier { get; } = 1.3M;
	protected override decimal EarlyPeriodDiscountMultiplier { get; } = 0.02M; 
	protected override decimal ExtendedPeriodDiscountMultiplier { get; } = 0.03M;
}