using FluentValidation;
using Services.Cover.Models;

namespace Claims;

internal class PolicyCoverValidator: AbstractValidator<PolicyCoverRequest>
{
	public PolicyCoverValidator()
	{
		RuleFor(c => c.StartDate)
			.Must(PolicyCover.DateIsNotInPast)
			.WithMessage("StartDate cannot be in the past.");

		RuleFor(c => c.EndDate)
			.Must(PolicyCover.DateIsNotInPast)
			.WithMessage("EndDate cannot be in the past.");

		RuleFor(c => c)
			.Must(c =>  PolicyCover.PeriodIsUnderYear(c.StartDate, c.EndDate))
			.WithMessage("Total insurance period cannot exceed 1 year");
	}
}