using FluentValidation;
using Services.Claim.Models;
using Services.Cover;
using Claims.DTOs;

namespace Claims;

internal class InsuranceClaimValidator: AbstractValidator<InsuranceClaimRequest>
{
	private readonly ICoverService _coverService;

	public InsuranceClaimValidator(ICoverService coverService)
	{
		_coverService = coverService;

		RuleFor(c => c)
			.Cascade(CascadeMode.Stop)
			.MustAsync(async (c, _) => await CoverExists(c.CoverId))
			.WithMessage("Cover does not exists")
			.MustAsync(async (c, _) => await CreatedIsInCoverPeriod(c.CoverId, c.Created))
			.WithMessage("Created Date must be within the Cover Period");

		RuleFor(c => c.DamageCost)
			.LessThanOrEqualTo(InsuranceClaim.MaximumDamageAllowed)
			.WithMessage("DamageCost cannot exceed 100.000");
	}

	private async Task<bool> CoverExists(string coverId)
	{
		var cover = await _coverService.GetCoverByIdAsync(coverId);
		return cover is not null;
	}
	
	private async Task<bool> CreatedIsInCoverPeriod(string coverId, DateTime created)
	{
		var cover = await _coverService.GetCoverByIdAsync(coverId);
		return InsuranceClaim.CreatedIsInCoverPeriod(cover!, created);
	}
}