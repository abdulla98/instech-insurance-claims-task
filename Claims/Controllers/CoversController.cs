using FluentValidation;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Services.Audit;
using Services.Cover;
using Services.Cover.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Claims.Controllers;

/// <summary>
/// Controller for Covers operations
/// </summary>
[PublicAPI]
[ApiController]
[Route("[controller]")]
public class CoversController : ControllerBase
{
    private readonly IValidator<PolicyCoverRequest> _validator;
    private readonly IAuditService _auditService;
    private readonly ICoverService _coverService;

    /// <summary>
    /// Constructor with the required dependency by dependency injection
    /// </summary>
    public CoversController(ICoverService coverService, IAuditService auditService,
        IValidator<PolicyCoverRequest> validator)
    {
        _coverService = coverService;
        _validator = validator;
        _auditService = auditService;
    }

    /// <summary>
    ///  Get a premium calculation with the desired query parameters 
    /// </summary>
    /// <param name="coverRequest"></param>
    [SwaggerResponse(200, "Result of the premium computation", typeof(decimal))]
    [SwaggerResponse(400, "Bad request")]
    [HttpPost("compute-premium")]
    public async Task<IActionResult> ComputePremiumAsync([FromQuery] PolicyCoverRequest coverRequest)
    {
        var validationResult = await _validator.ValidateAsync(coverRequest);
        if (!validationResult.IsValid) {
            return BadRequest(validationResult.Errors.ToArray());
        }
        
        var premium = _coverService.ComputePremium(coverRequest.StartDate, coverRequest.EndDate, coverRequest.CoverType);
        return Ok(premium);
    }

    /// <summary>
    /// Get all Covers
    /// </summary>
    [SwaggerResponse(200, "List Covers", typeof(IEnumerable<PolicyCover>))]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<PolicyCover>>> GetAsync()
    {
        var results = await _coverService.GetAllCoversAsync();
        return Ok(results);
    }

    /// <summary>
    /// Get Cover by id
    /// </summary>
    /// <param name="id">Id of the desired Cover</param>
    [SwaggerResponse(404, "Cover not found")]
    [SwaggerResponse(200, "Cover", typeof(PolicyCover))]
    [HttpGet("{id}")]
    public async Task<ActionResult<PolicyCover>> GetAsync(string id)
    {
        var result = await _coverService.GetCoverByIdAsync(id);
        if (result is null) {
            return NotFound();
        }

        return Ok(result);
    }

    /// <summary>
    /// Create a new Cover
    /// </summary>
    /// <param name="createCoverRequest">Cover to be created</param>
    [SwaggerResponse(200, "Returns Cover", typeof(PolicyCover))] 
    [SwaggerResponse(400, "Bad request")]
    [HttpPost]
    public async Task<ActionResult> CreateAsync([FromBody]PolicyCoverRequest createCoverRequest)
    {
        var validationResult = await _validator.ValidateAsync(createCoverRequest);
        if (!validationResult.IsValid) {
            return BadRequest(validationResult.Errors.ToArray());
        }

        var cover = new PolicyCover(createCoverRequest.StartDate, createCoverRequest.EndDate, createCoverRequest.CoverType);
        await _coverService.CreateCoverAsync(cover);

        await _auditService.AuditCover(cover.Id, "POST");
        return Ok(cover);
    }

    /// <summary>
    /// Delete a Cover
    /// </summary>
    /// <param name="id">Id of the Cover to be deleted</param>
    [SwaggerResponse(202, "Cover Deleted")]
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteAsync(string id)
    {
        await _auditService.AuditCover(id, "DELETE");
        await _coverService.DeleteCoverAsync(id);
        return NoContent();
    }
}