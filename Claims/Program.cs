using System.Text.Json.Serialization;
using Claims;
using Claims.DTOs;
using FluentValidation;
using Infrastructure;
using Services;
using Services.Audit;
using Services.Claim;
using Services.Cover;
using Services.Interfaces;
using Services.Claim.Models;
using Services.Cover.Models;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers().AddJsonOptions(x =>
    {
        x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    }
);

var cosmosDbConfig = new CosmosDbConfiguration();
builder.Configuration.Bind("CosmosDb", cosmosDbConfig);
var claimRepository = InitializeCosmosClientInstanceAsync<InsuranceClaim>(
        cosmosDbConfig.DatabaseName, 
        cosmosDbConfig.ContainerName,
        cosmosDbConfig.Account, 
        cosmosDbConfig.Key)
    .GetAwaiter()
    .GetResult();
var coverRepository = InitializeCosmosClientInstanceAsync<PolicyCover>(
        cosmosDbConfig.DatabaseName,
        "Cover",
        cosmosDbConfig.Account, 
        cosmosDbConfig.Key)
    .GetAwaiter()
    .GetResult();

builder.Services.AddScoped<IClaimService, ClaimService>((_) =>
{
    return new ClaimService(claimRepository);
});

builder.Services.AddScoped<ICoverService>((_) =>
{
    return new CoverService(coverRepository);
});

builder.Services.AddScoped<IAuditRepository, FileAuditRepository>();
builder.Services.AddScoped<IAuditService, AuditService>();
builder.Services.AddScoped<IValidator<PolicyCoverRequest>, PolicyCoverValidator>();
builder.Services.AddScoped<IValidator<InsuranceClaimRequest>, InsuranceClaimValidator>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options => {
    var xmlFilename = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
    options.EnableAnnotations();
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

static async Task<IRepository<T>> InitializeCosmosClientInstanceAsync<T>(string dbName, string containerName, 
    string account, string key)
where T: class, IIdMarker
{
    Microsoft.Azure.Cosmos.CosmosClient client = new Microsoft.Azure.Cosmos.CosmosClient(account, key);
    var cosmosDbService = new Repository<T>(client, dbName, containerName);
    Microsoft.Azure.Cosmos.DatabaseResponse database = await client.CreateDatabaseIfNotExistsAsync(dbName);
    await database.Database.CreateContainerIfNotExistsAsync(containerName, "/id");

    return cosmosDbService;
}

public partial class Program { }